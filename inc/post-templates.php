<?php
/**
 * Remove potentially duplicated hero image after upgrade to v0.4
 *
 * The changes to the content in this function should eventually be made
 * perminant in the database. (@see https://github.com/INN/Largo/issues/354)
 *
 * @since 0.4
 *
 * @param String $content the post content passed in by WordPress filter
 * @return String filtered post content.
 */
function cc_largo_remove_hero($content) {

	global $post;

	// 1: Only worry about this if it's a single template, there's a feature image,
	// we haven't overridden the post display and we're not using the classic layout.

	if( !is_single() ) 
		return $content;

	if( !has_post_thumbnail() ) 
		return $content;

	$options = get_post_custom($post->ID);

	if( isset($options['featured-image-display'][0]) )
		return $content;

	/*
	 * CHANGE
	 *
	 * In largo, this quits on the 'classic' template, so here we want to run it only on the classic template
	 */
	if( of_get_option( 'single_template' ) != 'classic' ) {
		return $content;
	}

	$p = explode("\n",$content);
	
	// 2: Find an image (regex)
	//
	// Creates the array:
	// 		$matches[0] = <img src="..." class="..." id="..." />
	//		$matches[1] = value of src.

	$pattern = '/<img\s+[^>]*src="([^"]*)"[^>]*>/';
	$hasImg = preg_match($pattern,$p[0],$matches);

	// 3: if there's no image, there's nothing to worry about.

	if( !$hasImg ) {
		return $content;
	}


	$imgDom = $matches[0];
	$src = $matches[1];

	// 4: Compare the src url to the feature image url.
	// If they're the same, remove the top image.

	$featureImgId = get_post_thumbnail_id();
	$pImgId = largo_url_to_attachmentid($matches[1]);

	// Try a second way to get the attachment id

	$pattern = '/class="([^"]+)"/';
	preg_match($pattern,$imgDom,$classes);

	$classes = $classes[1];

	if( !$pImgId ) {
		$pattern = '/wp-image-(\d+)/';
		preg_match($pattern,$classes,$imgId);
		$pImgId = $imgId[1];
	}

	if( !($pImgId == $featureImgId) ) 
		return $content;
	
	// 5: Check if it's a full width image, or if the image is not large enough to be a hero.

	if( strpos($classes,'size-small') || strpos($classes,'size-medium') ) 
		return $content;
	
	// 6: Else, shift the first paragraph off the content and return.

	array_shift($p);
	$content = implode("\n",$p);
	
	return $content;

}
add_filter('the_content','cc_largo_remove_hero',1);

