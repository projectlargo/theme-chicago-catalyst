<?php
/*
 * Catalyst Most Recent
 * Fetches N most recent stores from the chosen category. Used for the 25th anniversary layout
 */
class catalyst_recent_widget extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' 	=> 'catalyst-recent',
			'description' 	=> __('Lists recent posts from a given category', 'largo')
		);
		parent::__construct( 'catalyst-recent-widget', __('Catalyst Recent Stories', 'largo'), $widget_ops);
	}

	function widget( $args, $instance ) {
		global $post, $wp_query;
		extract( $args );

		$title = apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Recent', 'largo' ) : $instance['title'], $instance, $this->id_base);
		$show_more_btn = $instance['show_more_btn'] ? TRUE : FALSE;

		echo $before_widget;

		if ( $title ) echo $before_title . $title . $after_title;

		if ( $instance['show_desc'] ) {
			echo '<div class="cat-description">';
			echo category_description( $instance['term'] );
			echo '</div>';
		}

		//default query args: by date, descending
		$args = array(
			'post_type' 		  => 'post',
			'order' 			    => 'DESC',
			'posts_per_page' 	=> $instance['number'],
			'tax_query'       => array(
				array(
					'taxonomy'    => 'category',
					'field'       => 'id',
					'terms'       => array( $instance['term'] ),
				),
			)
		);

		$recent_query = new WP_Query( $args );

		while ( $recent_query->have_posts() ) : $recent_query->the_post();
			get_template_part( 'partials/content', 'widget' );
		endwhile;

		wp_reset_postdata();

		if ( $show_more_btn && $instance['title_link'] ) {
			printf(
				'<div class="show-more"><a class="btn btn-primary" href="%s" title="%s">More</a></div>',
				$instance['title_link'],
				$instance['title']
			);
		}

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']         = sanitize_text_field( $new_instance['title'] );
		$instance['term']          = sanitize_title( $new_instance['term'] );
		$instance['number']        = absint( $new_instance['number'] );
		$instance['show_desc']     = $new_instance['show_desc'];
		$instance['show_more_btn'] = $new_instance['show_more_btn'];
		return $instance;
	}

	function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Recent', 'term' => '', 'number' => 3 ) );
		$title = esc_attr( $instance['title'] );
		$num = absint( $instance['number'] );
		?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title', 'catalyst' ); ?>:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

			<p>
				<input type="checkbox" id="<?php echo $this->get_field_id('show_desc'); ?>" name="<?php echo $this->get_field_name('show_desc'); ?>" value="1" <?php checked($instance['show_desc'], 1); ?> >
				<label for="<?php echo $this->get_field_id('show_desc'); ?>"><?php _e('Output category description above posts', 'catalyst'); ?></label>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Posts to Display', 'catalyst'); ?>:</label>
				<select name="<?php echo $this->get_field_name('number'); ?>" id="<?php echo $this->get_field_id('number'); ?>">
				<?php
				for ($i = 1; $i <= 10; $i++) {
					echo '<option value="', $i, '"', selected($num, $i, FALSE), '>', $i, '</option>';
				} ?>
				</select>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('term'); ?>"><?php _e('From Category', 'catalyst'); ?>:</label>
				<?php wp_dropdown_categories( array(
					'selected' => $instance['term'],
					'name' => $this->get_field_name('term'),
					'id' => $this->get_field_id('term')
				)); ?>
			</p>

			<p>
				<input class="checkbox" type="checkbox" <?php checked($instance['show_more_btn'], 'on'); ?> id="<?php echo $this->get_field_id('show_more_btn'); ?>" name="<?php echo $this->get_field_name('show_more_btn'); ?>" /> 
				<label for="<?php echo $this->get_field_id('show_more_btn'); ?>">Show &quot;More&quot; button. <small class="description">Button will re-use the &quot;Widget Title Link&quot;, if set.</small></label>
			</p>
		<?php
	}
}
