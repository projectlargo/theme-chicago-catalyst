<?php
/*
 * List series posts categorized as "infographics"
 *
 * This widget gets the post thumbnails and post titles for posts in the current series landing page's series that are also in the "infographics" category.
 * This widget only outputs on series landing pages.
 *
 * This widget is not duplicable with Largo's widgets.
 *
 * @since 1.0
 * @since Largo 0.4
 * @link https://github.com/INN/Largo/blob/master/inc/wp-taxonomy-landing/functions/cftl-series-order.php This handles some of the stranger query vars.
 */
class catalyst_infographics_widget extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' 	=> 'catalyst-infographics',
			'description' 	=> __('Lists infographics from the current issue', 'largo')
		);
		parent::__construct( 'catalyst-infographics-widget', __('Catalyst Issue Infographics', 'largo'), $widget_ops);
	}

	function widget( $args, $instance ) {
		// only useful on issue pages
		if ( !is_singular('cftl-tax-landing') ) {
			return;
		}

		global $post, $wp_query;
		extract( $args );

		$title = apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Infographics', 'largo' ) : $instance['title'], $instance, $this->id_base);

		ob_start();

		echo $before_widget;

		if ( $title ) echo $before_title . $title . $after_title;

		// Get the settings from this series' per-series settings, which are set on the series' landing page
		$opt = get_post_custom( $post->ID );
		foreach( $opt as $key => $val ) {
			$opt[ $key ] = $val[0];
		}

		//get the posts for this series, categorized as infographics
		//default query args: by date, descending
		$args = array(
			'post_type' 		  => 'post',
			'order' 			    => 'DESC',
			'posts_per_page' 	=> 99,
			'tax_query'       => array(
				'relation'      => 'AND',
				array(
					'taxonomy'    => 'series',
					'field'       => 'slug',
					'terms'       => array( $wp_query->query_vars['term'] ),
				),
				array(
					'taxonomy'    => 'category',
					'field'       => 'slug',
					'terms'       => array( 'infographic' ),
				),
			)
		);

		/**
		 * Change the query args for inforgraphics posts according to how posts are ordered in the series
		 * These unusual WP_Query args are handled by filters defined in Largo
		 * @link https://github.com/INN/Largo/blob/master/inc/wp-taxonomy-landing/functions/cftl-series-order.php
		 */
		switch ( $opt['post_order'] ) {
			case 'ASC':
				$args['order'] = 'ASC';
				break;
			case 'custom':
				$args['orderby'] = 'series_custom';
				break;
			case 'featured, DESC':
			case 'featured, ASC':
				$args['orderby'] = $opt['post_order'];
				break;
		}

		$igraphics_query = new WP_Query( $args );

		// and finally wind the posts back so we can go through the loop as usual
		echo "<ul>";
		while ( $igraphics_query->have_posts() ) : $igraphics_query->the_post();
			echo "<li>";
			$link = get_permalink();
			echo '<a href="', $link, '" target="_top">', get_the_title();
			echo wp_get_attachment_image( get_post_thumbnail_id(), "medium", false );
			echo "</a>";
			echo "</li>";
		endwhile;
		echo "</ul>";

		wp_reset_postdata();
		echo $after_widget;
		$return = ob_get_clean();
		
		// Only output the widget if there's posts in the query.
		if ( $igraphics_query->post_count > 0 ) {
			echo $return;
		}

		return $return;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Infographics' ) );
		$title = esc_attr( $instance['title'] );
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title', 'largo' ); ?>:</label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
	<?php
	}
}
