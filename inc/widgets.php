<?php
/**
 * Functions for widgets
 *
 * This file was previously named catalyst-widgets.php, in the theme root.
 * @since 1.1
 */

/**
 * Register widgets
 *
 * @since 1.1
 * @since Largo 0.5.5
 */
function catalyst_widgets() {
	require_once( 'widgets/catalyst_infographics_widget.php' );
	require_once( 'widgets/catalyst_recent_widget.php' );
	register_widget( 'catalyst_infographics_widget' );
	register_widget( 'catalyst_recent_widget' );
}
add_action( 'widgets_init', 'catalyst_widgets', 20 );
