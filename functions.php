<?php
/**
 * Chicago Catalyst Largo overrides and whatnot
 */
define( 'INN_MEMBER', true );

/**
 * Require the files
 *
 * @since 1.0
 * @since Largo 0.4
 */
// load custom widgets
require_once('inc/widgets.php');
// load custom shortcode
require_once('inc/shortcodes.php');
// fix for potential duplicate hero images
require_once('inc/post-templates.php');


/**
 * Add typekit fonts
 *
 * @since 1.0
 * @since Largo 0.4
 */
function catalyst_typekit() {
	if ( !is_admin() ) {
	?>
		<script src="//use.typekit.net/cqb8kdk.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<?php
	}
}
add_action( 'wp_head', 'catalyst_typekit' );


/**
 * Enqueue theme javascript
 *
 * @since 1.0
 * @since Largo 0.4
 */
function catalyst_javascript() {
	wp_enqueue_script( 'catalyst', get_stylesheet_directory_uri() . '/js/catalyst.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'catalyst_javascript' );

/**
 * enqueue theme stylesheet
 *
 * @since 1.1
 * @since Largo 0.5.4
 */
function catalyst_styles() {
	$suffix = (LARGO_DEBUG)? '' : '.min';

	wp_dequeue_style( 'largo-child-styles' );
	wp_enqueue_style( 'catalyst-styles', get_stylesheet_directory_uri().'/css/style' . $suffix . '.css' );
}
add_action( 'wp_enqueue_scripts', 'catalyst_styles', 20 );


// homepage layout
function catalyst_register_homepage_layouts() {
	// Load layouts from `layouts/`
	$layouts = glob( get_stylesheet_directory() . '/homepages/layouts/*.php' );
	foreach ($layouts as $layout) {
		include_once $layout;
	}
	$layouts = array('Catalyst');

	foreach ( $layouts as $layout ) register_homepage_layout($layout);
}
// Largo homepage initialization uses priority 0.
// Child layout registration must be < 10 for post prominence tags to function
add_action( 'init', 'catalyst_register_homepage_layouts', 4 );


/**
 * Register Catalyst stuff
 */
function catalyst_init() {
	// Register additional sidebars
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => __( 'Homepage Middle', 'catalystchicago' ),
			'id' => 'catalyst-homepage-middle',
			'description' => __( 'The primary widget area', 'catalystchicago' ),
			'before_widget' => '<aside id="%1$s" class="widget-container span6 %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title ruled">',
			'after_title' => '</h2>',
		));
		// former 'homepage_right_top' sidebar replaced with largo-native 'header-widget-area'
	}

	// homepage images
	add_image_size( 'catalyst_featured', 180, 130, true );
	set_post_thumbnail_size( 175, 175, false ); // thumbnail

}
add_action( 'init', 'catalyst_init', 20 );


/**
 * Add 'featured in magazine' term
 *
 * @since 1.0
 * @since Largo 0.4
 */
function catalyst_prominence( $terms ) {
	$terms[] = array(
		'name' => __('Featured in Magazine', 'largo'),
		'description' => __('Makes post eligible to appear in Magazine section of Catalyst homepage', 'largo'),
		'slug' => 'magazine'
	);
	return $terms;
}
add_filter('largo_prominence_terms', 'catalyst_prominence', 10);

/**
 * featured image shortcode for archives
 * Implemented because all the Landing Page posts have [featured_image] in their body
 *
 * @since 1.0
 * @since Largo 0.4
 */
function thumb_shortcode( $atts ) {
	$options = shortcode_atts( array(
		'size' => 'medium',
	), $atts );

	global $post;

	the_post_thumbnail( $options['size'] );
}
add_shortcode( 'featured_image', 'thumb_shortcode' );

/**
 * Filter the_title() for cftl-tax-landings to include a publication date if main
 *
 * @since 1.0
 * @since Largo 0.4
 */
function catalyst_issue_title( $title, $id ) {
	global $post;
	if ( get_post_type( $id ) == 'cftl-tax-landing' && $post->ID != 80756 ) {

		$year = get_the_date('Y');
		$month = get_the_date('n');
		$season = get_the_date('F');
		// for issues starting Sept/Oct 2009, convert month to season
		if ( get_the_date('U') >= 1251763200 ) {
			switch($month) {
				case "1":
				case "2":
				case "12":
					$month = "Winter";
					break;
				case "3":
				case "4":
				case "5":
					$month = "Spring";
					break;
				case "6":
				case "7":
				case "8":
					$month = "Summer";
					break;
				case "9":
				case "10":
				case "11":
					$month = "Fall";
					break;
			}
		} else {
			$month = $season;
		}
		return $title . ": $month $year";
	} else {
		return $title;
	}
}
add_filter( 'the_title', 'catalyst_issue_title', 10, 2 );

/**
 * filter Largo's Load More Posts query on the homepage to only return posts in the "Homepage Featured" taxonomy
 *
 * @since 1.0
 * @since Largo 0.5.2
 */
function catalyst_lmp_args($args) {
	if ( isset($_POST['is_home']) && $_POST['is_home'] == 'true' ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'prominence',
				'field'    => 'slug',
				'terms'    => 'hide-from-homepage',
				'operator' => 'NOT IN',
			)
		);
	}
	return $args;
}
add_filter('largo_lmp_args', 'catalyst_lmp_args');


/**
* chicago_series_landing_link
*
* Filter post permalinks for the Landing Page custom post type.
* Replace direct post link with the link for the associated
* Series taxonomy term, using the most recently created term
* if multiple are set.
*
* This filter overrides Largo's wp-taxonomy-landing filter,
* which attempts to use the link for ANY term from ANY taxonomy.
* Largo really only cares about the Series taxonomy.
*
* @since 1.0
* @since Largo 0.4
*/
function chicago_series_landing_link($post_link, $post) {
	// Only process Landing Page post type
	if ("cftl-tax-landing" == $post->post_type) {
		// Get all series taxonomy terms for this landing page
		$series_terms = wp_get_object_terms(
			$post->ID,
			'series',
			array('orderby' => 'term_id', 'order' => 'DESC', 'fields' => 'slugs')
		);
		// Only proceed if we successfully found at least 1
		// series term for the landing page
		if ( !is_wp_error( $series_terms ) && !empty($series_terms) ) {
			// Get the link for the first series term
			// (ordered by the highest ID in the case of multiple terms)
			$term_link = get_term_link( $series_terms[0], 'series' );
			// Only proceed if we successfully found the term link
			if ( !is_wp_error( $term_link ) && strlen(trim($term_link)) ) {
				$post_link = esc_url($term_link);
			}
		}
	}
	// Return the filtered link
	return $post_link;
}
// Largo's wp-taxonomy-landing library filters at priority 10.
// We must filter AFTER that.
add_filter('post_type_link', 'chicago_series_landing_link', 20, 2);


/**
 * Add option for front-page subscribe button, and other things
 */
function catalyst_custom_options( $options ) {
	// tacking this option on to the end of the theme options
	$options[] = array(
		'name' => __('Catalyst Options', 'largo'),
		'type' => 'heading');
	$options[] = array(
		'name' => __('Catalyst Homepage Options', 'largo'),
		'type' => 'info');	
	$options[] = array(
		'desc' 		=> __('Show "From The Magazine" section on homepage?</strong>', 'largo'),
		'id' 		=> 'cc_show_magazine_home',
		'std' 		=> '1',
		'type' 		=> 'checkbox');
	return $options;
}
add_filter( 'largo_options', 'catalyst_custom_options');
