

## compile less files from bash (shell)
## v 0.0.1-SNAPSHOT
#########################################################################
## This script will compile ONE less-file to ONE css-file
## allow to compile "once" or "watch"
#########################################################################
## DEPENDENCIES:
## node                  http://nodejs.org/
## npm                   http://nodejs.org/
## BASH                  http://www.gnu.org/software/bash/
## observr               https://github.com/kevinburke/observr
## terminal-notifier     https://github.com/alloy/terminal-notifier
## lessc                 https://github.com/less/less.js/ (using v 1.5.1)
#########################################################################
## $ (sudo) gem install observr
## $ (sudo) gem install terminal-notifier
## $ npm install (-g) less

## usage: lessc [option option=parameter ...] <source> [destination]
##
## If source is set to `-' (dash or hyphen-minus), input is read from stdin.
##
## options:
##   -h, --help               Print help (this message) and exit.
##   --include-path=PATHS     Set include paths. Separated by `:'. Use `;' on Windows.
##   -M, --depends            Output a makefile import dependency list to stdout
##   --no-color               Disable colorized output.
##   --no-ie-compat           Disable IE compatibility checks.
##   --no-js                  Disable JavaScript in less files
##   -l, --lint               Syntax check only (lint).
##   -s, --silent             Suppress output of error messages.
##   --strict-imports         Force evaluation of imports.
##   --insecure               Allow imports from insecure https hosts.
##   -v, --version            Print version number and exit.
##   -x, --compress           Compress output by removing some whitespaces.
##   --clean-css              Compress output using clean-css
##   --source-map[=FILENAME]  Outputs a v3 sourcemap to the filename (or output filename.map)
##   --source-map-rootpath=X  adds this path onto the sourcemap filename and less file paths
##   --source-map-basepath=X  Sets sourcemap base path, defaults to current working directory.
##   --source-map-less-inline puts the less files into the map instead of referencing them
##   --source-map-map-inline  puts the map (and any less files) into the output css file
##   --source-map-url=URL     the complete url and filename put in the less file
##   -rp, --rootpath=URL      Set rootpath for url rewriting in relative imports and urls.
##                            Works with or without the relative-urls option.
##   -ru, --relative-urls     re-write relative urls to the base less file.
##   -sm=on|off               Turn on or off strict math, where in strict mode, math
##   --strict-math=on|off     requires brackets. This option may default to on and then
##                            be removed in the future.
##   -su=on|off               Allow mixed units, e.g. 1px+1em or 1px*1px which have units
##   --strict-units=on|off    that cannot be represented.
##   --global-var='VAR=VALUE' Defines a variable that can be referenced by the file.
##   --modify-var='VAR=VALUE' Modifies a variable already declared in the file.
##
## -------------------------- Deprecated ----------------
##   -O0, -O1, -O2            Set the parser's optimization level. The lower
##                            the number, the less nodes it will create in the
##                            tree. This could matter for debugging, or if you
##                            want to access the individual nodes in the tree.
##   --line-numbers=TYPE      Outputs filename and line numbers.
##                            TYPE can be either 'comments', which will output
##                            the debug info within comments, 'mediaquery'
##                            that will output the information within a fake
##                            media query which is compatible with the SASS
##                            format, and 'all' which will do both.
##   --verbose                Be verbose.
##
## Report bugs to: http://github.com/less/less.js/issues
## Home page: <http://lesscss.org/>

#########################################################
################### START CONFIG ########################

## set files and path ##############
## modify these to fit your needs ##

## locaion of less files ###########
lessPath="less"

## locaion of css files ############
cssPath="css"

## name of less file ###############
inputFile="catalyst.less"

## name of css files ###############
outputFile="catalyst.css"

#################### END CONFIG #########################
#########################################################

#########################################################
## NO MODIFICATIONS SHOULD BE NEEDED BEYOND THIS POINT ##

#########################################################
################### START SCRIPT ########################

## name of script
SCRIPTNAME="$0"

## build filePath + fileName
INPUT="${lessPath}/${inputFile}"
OUTPUT="${cssPath}/${outputFile}"

## flag to check if command was sucessfully executed
COMPLETED="NO"

## helpers for logging
LINEBREAK=$(for i in $(seq 1 80); do printf "-"; done)

## define PARAMETER
PARAMETER=""

if [ -n "${2}" ] ; then
	if [ ${2} = "-m" ] || [ ${2} = "-mediaquery" ] ; then
		PARAMETER="--line-numbers=mediaquery"
	elif [ ${2} = "-c" ] || [ ${2} = "-comments" ] ; then
		PARAMETER="--line-numbers=comments"
	elif [ ${2} = "-a" ] || [ ${2} = "-all" ] ; then
		PARAMETER="--line-numbers=all"
	fi
fi


## notify on compilation success
## works for once
NOTIFYTITLE="Sucess"
NOTIFYSUBTITLE="$(date)"
NOTIFYMESSAGE="Compiled: ${inputFile}"

function notify () {
	terminal-notifier -title "${NOTIFYTITLE}" -subtitle "${NOTIFYSUBTITLE}" -message "${NOTIFYMESSAGE}"
}
## works for watch
notify="terminal-notifier \
-title \"${NOTIFYTITLE}\" \
-subtitle \"${NOTIFYSUBTITLE}\" \
-message \"${NOTIFYMESSAGE}\""



## will compile files with given parameters
function compile-less () {
	echo "start compiling less files"
	echo "${LINEBREAK}"
	lessc ${PARAMETER} ${INPUT} ${OUTPUT} && COMPLETED="YES"
	if [ ${COMPLETED} = "YES" ] ; then
		echo "${INPUT} > ${OUTPUT}"
		echo "${LINEBREAK}"
		# notify
	else
		echo "seems like something went wrong"
	fi
}
## will compile files with given parameters
## and then watch for changes
function watch-less () {
	## fist compile the files
	compile-less
	## use observr to watch files
	observr -e "; \
	puts 'start watching less files'; \
	puts '-' * 80; \
	watch('.*\.less') \
	{ cmd = 'lessc ${PARAMETER} ${INPUT} ${OUTPUT}'; \
	log = 'echo \"${INPUT} > ${OUTPUT}\" && ${notify}'; \
	o = \`#{cmd}\`; \
	l = \`#{log}\`; \
	puts o << l;}"
}

## provide error message
function illegal-option (){
	echo "illegal option"
	echo "use '--help' to show options"
}

## output help when requested
function show-help () {
	echo "${SCRIPTNAME} [OPTION] [PARAMETER]"
	echo ""
	echo "OPTION---------------------HELP-------------------------------"
	echo "watch                      watch files"
	echo "once                       compile files once"
	echo ""
	echo "PARAMETER------------------HELP-------------------------------"
	echo "-c, --comments             output comments"
	echo "-m, --mediaquery           output mediaquery"
	echo "-a, --all                  output comments & mediaquery"
	echo ""
	echo "--------------------------------------------------------------"
	echo "-h, --help                 Print help (this message) and exit."
	echo "--------------------------------------------------------------"
}

## keep this small and modular ##
#################################

## only execute if option is set
if [ -n "${1}" ] ; then
	echo ""
	if [ ${1} = "-h" ] || [ ${1} = "--help" ] ; then
		show-help
	elif [ ${1} = "watch" ] ; then
		watch-less
	elif [ ${1} = "once" ] ; then
		compile-less
	else
		illegal-option
	fi
## otherwise display help #########
else
	show-help
fi

#################### END SCRIPT #########################
#########################################################
