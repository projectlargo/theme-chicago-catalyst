<?php
class Catalyst extends Homepage {
	var $name = 'Catalyst Chicago';
	var $type = 'catalyst';
	var $description = 'Homepage layout for Catalyst Chicago, by Cornershop Creative';
	var $sidebars = array(
		'Homepage Left Rail (An optional widget area that, when enabled, appears to the left of the main content area on the homepage)'
	);
	var $rightRail = true;

	function __construct($options=array()) {
		$defaults = array(
			'template' => get_stylesheet_directory() . '/homepages/templates/catalyst.php',
			'assets' => array(
				array('voiceofoc', get_stylesheet_directory_uri() . '/homepages/assets/css/catalyst.css', array()),
			)
		);
		$options = array_merge($defaults, $options);
		$this->init($options);
		$this->load($options);
	}

	/**
	 * Register all the prominence terms
	 */
	public function init($options=array()) {
		$this->prominenceTerms = array(
			array(
				'name' => __('Top Story', 'largo'),
				'description' 	=> __('Add this label to a post to make it a Top Story on the homepage', 'largo'),
				'slug' 			=> 'top-story'
			),
			array(
				'name' => __('Homepage Featured', 'largo'),
				'description' => __('Add this label to posts to display them on the homepage.', 'largo'),
				'slug' => 'homepage-featured'
			),
			array(
				'name' => __('In Case You Missed It', 'largo'),
				'description' => __('Add this label to posts to display them in the In Case You Missed It section of the homepage. Story must also include a Featured Image.', 'largo'),
				'slug' => 'missed-it'
			),
			array(
				'name' => __('Hide From Homepage', 'catalyst'),
				'description' => __('Prevent this post from appearing on the homepage', 'catalyst'),
				'slug' => 'hide-from-homepage'
			),
		);
	}

	/**
	* Override base Homepage class setRightRail method
	* Disable the Largo right rail, as we will implement our own.
	*/
	public function set_right_rail() {
		global $largo;
		unset($largo['home_rail']);
	}

	/**
	 * Homepage Top Story section
	 * Above the fold, center section. Most recent 'top-story' with an image
	 */
	public function catalyst_top_story() {
		global $shown_ids, $post;
		$content = '';
		//
		// Sticky Top Story post
		//
		$sticky_topstory = largo_get_featured_posts( array(
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy'	=> 'prominence',
					'field'		=> 'slug',
					'terms'		=> 'top-story'
				),
				array(
					'taxonomy' => 'prominence',
					'field'    => 'slug',
					'terms'    => 'hide-from-homepage',
					'operator' => 'NOT IN',
				)
			),
			'ignore_sticky_posts'	=> TRUE,
			'showposts'				=> 1
		) );
		if ( $sticky_topstory->have_posts() ) {
			while ( $sticky_topstory->have_posts() ) {
				$sticky_topstory->the_post();
				$shown_ids[] = get_the_ID();
				$sticky_ts_content = array();
				// Photo
				if ( has_post_thumbnail() ) {
					$sticky_ts_content['photo-creditor'] = navis_get_media_credit( get_post_thumbnail_id() );
					if ( $sticky_ts_content['photo-creditor']->to_string() ) {
						$sticky_ts_content['photo-credit'] = sprintf('<figcaption class="photo-credit">%s</figcaption>', $sticky_ts_content['photo-creditor']->to_string());
					}
					$ts_hero = wp_prepare_attachment_for_js( get_post_thumbnail_id() );
					$sticky_ts_content['photo'] = sprintf(
						'<figure>%s%s<div class="hero-caption">%s</div></figure>',
						get_the_post_thumbnail( get_the_ID(), 'large' ),
						$sticky_ts_content['photo-credit'],
						$ts_hero['caption']
					);
				} else {
					$sticky_ts_content['photo'] = "";
				}

				// Header
				$sticky_ts_content['header'] = sprintf(
					'<header><h1><a href="%s">%s</a></h1><span class="date">%s</span><h5 class="byline">%s</h5></header>',
					get_permalink(),
					get_the_title(),
					largo_time( false ),
					largo_byline( false, true )
				);
				// Excerpt
				$sticky_ts_content['excerpt'] = largo_excerpt( $post, 4, FALSE, '', FALSE );
				// Append story to content
				$content .= sprintf(
					'<article id="story-%d">%s%s%s</article>',
					get_the_ID(),
					$sticky_ts_content['photo'],
					$sticky_ts_content['header'],
					$sticky_ts_content['excerpt']
				);
			} // while ( $sticky_topstory->have_posts() )
		} // if ( $sticky_topstory->have_posts() )
		else {
			$content .= '<article class="catalyst-missing"><header><h1>No Top Story</h1></header><p class="excerpt">No top story w/image found. Please contact the editor and web team.</p></article>';
		}
		return $content;
	}

	/**
	 * Homepage Secondary section
	 * Above the fold, left section. Three stories, 'homepage-featured'. Needn't have images.
	 */
	public function catalyst_secondary() {
		global $shown_ids, $post;
		$content = '';
		ob_start();
		$featstories = largo_get_featured_posts( array(
			'tax_query' => array(
				array(
					'taxonomy' => 'prominence',
					'field' => 'slug',
					'terms'    => 'hide-from-homepage',
					'operator' => 'NOT IN',
				)
			),
			'ignore_sticky_posts' => TRUE,
			'showposts' => 3,
			'post__not_in' => $shown_ids,
		) );
		if ( $featstories->have_posts() ) {
			while ( $featstories->have_posts() ) {
				$featstories->the_post();
				$shown_ids[] = get_the_ID();
				?>
				<article id="story-<?php echo get_the_ID(); ?>">
					<h5 class="top-tag"><?php largo_top_term(); ?></h5>
					<?php
						if ( has_post_thumbnail( get_the_ID() ) ) { ?>
							<figure><?php the_post_thumbnail( 'catalyst_featured' ); ?></figure>
						<?php	}
					?>
					<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
					<span class="date"><?php largo_time(); ?></span>
					<h5 class="byline"><?php largo_byline( true, true ); ?></h5>
					<?php largo_excerpt( $post, 1, FALSE ); ?>
				</article><?php
			} // while ( $sticky_featstory->have_posts() )
		} // if ( $sticky_featstory->have_posts() )
		else {
			echo '<article class="catalyst-featured"><header><h1>No Featured Stories</h1></header><p class="excerpt">No "homepage featured" stories found. Please contact the editor and web team.</p></article>';
		} // else ( $sticky_featstory->have_posts() )

		return ob_get_clean();
	}

	/**
	 * Homepage Magazine section
	 * Below the fold, left section. All include feature images
	 *
	 *    article.span4 (x9)
	 *        figure
	 *            img
	 *        h1
	 *
	 */
	public function catalyst_magazine() {
		global $shown_ids, $post;
		$content = '';
		$issues_objs = get_terms( 'series' );
		$issues_terms = array();
		foreach ( $issues_objs as $issues_obj ) {
			$issues_terms[] = $issues_obj->slug;
		}

		$magazine_story = largo_get_featured_posts( array(
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy'	=> 'prominence',
					'field'		=> 'slug',
					'terms'		=> 'magazine'
				),
				array(
					'taxonomy'	=> 'series',
					'field'		=> 'slug',
					'terms'		=> $issues_terms
				),
				array(
					'taxonomy' => 'prominence',
					'field'    => 'slug',
					'terms'    => 'hide-from-homepage',
					'operator' => 'NOT IN',
				)
			),
			'ignore_sticky_posts'	=> FALSE,
			//'meta_key'				=> '_thumbnail_id',
			'showposts'				=> 1,
			'post__not_in'			=> $shown_ids
		) );

		if ( $magazine_story->have_posts() ) {
			while ( $magazine_story->have_posts() ) {
				$magazine_story->the_post();
				$shown_ids[] = get_the_ID();
				// Photo
				if ( has_post_thumbnail() ) {
					$magazine_content['photo'] = sprintf(
						'<figure><a href="%s">%s</a></figure>',
						get_permalink(),
						get_the_post_thumbnail( get_the_ID(), 'catalyst_featured' )
					);
				} else {
					$magazine_content['photo'] = "";
				}
				// Header
				$magazine_content['header'] = sprintf(
					'<header><h1><a href="%s">%s</a></h1><span class="date">%s</span><h5 class="byline">%s</h5></header>',
					get_permalink(),
					get_the_title(),
					largo_time( false ),
					largo_byline( false, true )
				);
				// Excerpt
				$magazine_content['excerpt'] = largo_excerpt( $post, 4, FALSE, '', FALSE );
				// Issue link
				$issues = get_the_terms( get_the_ID(), 'series' );
				if ( is_array($issues) ) {
					$issue = array_shift($issues);
					$magazine_content['more'] = sprintf('<footer><a href="%s" class="more">%s</a></footer>',
						get_term_link( $issue->slug, 'series' ),
						__('More from this issue', 'catalystchicago')
					);
				} else {
					$magazine_content['more'] = "";
				}
				// Append story to content
				$content .= sprintf(
					'<div class=""><article id="story-%d"><h5 class="top-tag">%s</h5>%s%s%s%s</article></div>',
					get_the_ID(),
					largo_top_term( array('echo' => FALSE) ),
					$magazine_content['photo'],
					$magazine_content['header'],
					$magazine_content['excerpt'],
					$magazine_content['more']
				);
			} // while ( $missedit_stories->have_posts() )
		} // if ( $missedit_stories->have_posts() )
		else {
			$content .= '<section class="row-fluid missed-it"><div class="span12"><header><h1>No &quot;Magazine&quot; Stories</h1></header><p class="excerpt">No &quot;In Case You Missed It&quot; stories found. Please contact the editor and web team.</p></div></section>';
		} // else ( $missedit_stories->have_posts() )

		return $content;
	}
}
