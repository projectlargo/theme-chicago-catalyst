<?php
/**
 * Home Template: Catalyst Chicago
 * Description: Homepage layout for Catalyst Chicago, by Cornershop Creative
 */

global $largo, $shown_ids, $tags;
?>
<main id="homepage-catalyst" class="row-fluid clearfix">
	<div class="row-fluid">
		<section class="homepage-top-story">
			<header>
				<h2 class="ruled"><?php _e( 'Latest News & Analysis', 'catalystchicago' ); ?></h2>
			</header>
			<?php
				echo $catalyst_top_story;
			?>
		</section>
	</div>
	<div class="row-fluid">
		<section class="homepage-top-pair span12">
			<?php
				echo $catalyst_secondary;
			?>
		</section>
	</div>
	<?php if ( of_get_option( 'cc_show_magazine_home', true ) ) : ?>
		<div class="row-fluid">
			<section class="magazine span12">
				<header>
					<h2 class="ruled"><?php _e( 'From the Magazine', 'catalystchicago' ); ?></h2>
				</header>
				<?php
					echo $catalyst_magazine;
				?>
			</section>
		</div>
	<?php endif; ?>
	<section class="row-fluid homepage-middle">
		<?php
			if ( is_active_sidebar( 'catalyst-homepage-middle' ) ) {
				dynamic_sidebar( 'catalyst-homepage-middle' );
			} else {
				echo "[widgets in here TBD]";
			}
		?>
	</section>
</main>
<h2 class="ruled"><?php _e( 'More Articles', 'catalystchicago' ); ?></h2>