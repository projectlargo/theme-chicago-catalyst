jQuery(document).ready(function($) {

	// assign i classes to parents in footer
	$('#ft-social i').each( function( index ) {
		var iconType = $(this).attr('class').replace('icon-', 'for-');
		$(this).parent('a').addClass( iconType );
	});

	// move 'related story' to article top
	if ( $('.single .catalyst-related-posts').length ) {
		$('.single .catalyst-related-posts').addClass('in-post').prependTo('.entry-content');
	}

	// move related posts widget title out of li
	if ( $('.single .largo-explore-related').length ) {
		$('.single .largo-explore-related h5:first').prependTo('.single .largo-explore-related');
		$('.single .largo-explore-related li:first').remove();
	}

	// disqus header
	if ( $('#content> #disqus_thread').length ) {
		$('<h5 class="disqus-header">Comments</h5>').insertBefore('#disqus_thread');
	}

	// Issue headers
	if ( $('#series-header').length ) {
		$('#series-header .entry-title').html( function( idx, html ) {
			return html.replace(/^(.*)\|(.*)$/, '$1 <span class="issue-publication-date">$2</span>');
		});
	}

	// make issue thumbnails in the landing page archive clickable
	$('.landing-archive li').each(function (index) {
		var theURL = $('a', $(this) ).first().attr('href');
		$('img', $(this) ).wrap('<a href="'+theURL+'"></a>');
	});

	// select year for landing pages
	$('#landing-year').on('change', function() {
		if ( $(this).val() != '' ) {
			window.location.search = "?issue-year=" + $(this).val();
		} else {
			window.location.search = '';
		}

	});

});
