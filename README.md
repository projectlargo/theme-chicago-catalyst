# Largo child theme for Catalyst Chicago

This repository contains Catalyst Chicago's Largo child theme, for deployment on INN's shared hosting.

Developed by [Cornershop Creative](http://cornershopcreative.com/)
Maintained by [The INN Nerds](http://nerds.inn.org/)

## Features

- To hide a post from the homepage, select "Hide From Homepage" in the available prominences.

## Migration notes for version 1.1, February 2016

1. In Theme Options > Advanced, enable the Header Widget Area.
2. Move widgets formerly in the custom sidebar in the header into the Largo-default Header Widget widget area
3. In Theme Options > Basic Settings, set the Copyright message to:

```html
<div id="boilerplate" class="row-fluid clearfix">
	<p class="footer-credit">
		Catalyst Chicago is an editorially independent print and online newsmagazine published by the <a href="http://www.communityrenewalsociety.org/">Community Renewal Society</a>. Visit our sister publication <a href="http://www.chicagoreporter.com/">The Chicago Reporter</a>. 111 W. Jackson Blvd., Suite 820, Chicago, IL 60604 â¢ (312) 427-4830 â¢ <a href="mailto:editor@catalyst-chicago.org">editor@catalyst-chicago.org</a>
	</p>
</div>
```

4. If you are a developer, you will need to run `npm install` to update dependencies.
5. In Appearance > Menus, add "terms and conditions of use" and "Privacy Policy" to the "Footer Navigation" menu.
6. Copy the content from http://catalyst-chicago-org.largoproject.staging.wpengine.com/catalyst-25th-anniversary/ into a new page, and set the title and custom sidebar to matching values.
7. In Tools > Redirection, add a new rule with the following properties:
	- Source URL: /series/25th-anniversary/
	- Match: URL only
	- Action: Redirect to URL
	- Target URL: /catalyst-25th-anniversary/
